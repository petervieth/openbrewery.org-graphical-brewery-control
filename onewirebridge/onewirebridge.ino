/*
  Arduino Yun OpenBrewery Onewire Bridge
  
  (C) 2015 Peter Vieth, openbrewery.org
 */

// For onewire temp sensor:
#include <OneWire.h>

// Turn debug mode on, more Console prints
#define DEBUG  0

// Define the temperatuer sensor pin.  Hard coded to F7
#define ONEWIRE_PIN  16
OneWire ds(ONEWIRE_PIN);

// This should actually be defined within arduino files...
//#define NUM_DIGITAL_PINS  19
#define NUM_ANALOG_PINS  10
#define MIN_TEMP 0
#define MAX_TEMP 100

/* 
  Initialization

  */

void setup() {
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  
  //digitalWrite(13, HIGH);
}
/*
  The program loop, which runs over and over again
  */
void loop() {

  #if (DEBUG>0)
  //Serial.println("looping...");
  #endif
  
  discoverOneWireDevices();

  delay(50); // Poll every 50ms
  #if(DEBUG>0)
  delay(3000);
  #endif
}




/*
  Read temp from OneWire temp sensors
  Returns the temperature in C x 100.  So 39.6C would be returned as 3960
  This is due to the difficulties of dealing with floats.
  */

void discoverOneWireDevices() {
  byte i;
  byte present = 0;
  //byte data[12];
  byte addr[8];
  
  // Search for the next device. The addrArray is an 8 byte array. If a device is found, addrArray is filled with the device's address and true is returned. 
  // If no more devices are found, false is returned.
  // If true, print the device addr
  int deviceCount=0;
  
  ds.reset();
  ds.reset_search();
  
  
  while(ds.search(addr)) {
    deviceCount++;
    
    // Base64 encode into plain English characters (and equals sign etc)
      

    if ( OneWire::crc8( addr, 7) != addr[7]) {
        Serial.println(" - CRC invalid!");
    }
    
    int tc100=readOneWireTemp(ds,addr);
    analogWrite(deviceCount-1, map(tc100, MIN_TEMP, MAX_TEMP, 0, 255));

    Serial.print("Read ");
    Serial.print(deviceCount);
    Serial.print("=");
    Serial.println(tc100);
    // the above really sucks
  } // while
  Serial.println("Done.");
  ds.reset_search();
  return;
}

/*
  Read temp from a OneWire temp sensor given its address
  Returns the temperature in C x 100.  So 39.6C would be returned as 3960
  This is due to the difficulties of dealing with floats.
  */


// Return temp in C * 100
int readOneWireTemp(OneWire ds, byte* addr) {
  
  byte i;
  byte present = 0;
  byte data[12];
  
  ds.reset();
  ds.select(addr);
  ds.write(0x44,1);         // start conversion, with parasite power on at the end

  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.

  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad

  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
    //Serial.print(data[i], HEX);
    //Serial.print(" ");
  }
  
  int HighByte, LowByte, TReading, SignBit, Tc_100; //, Whole, Fract;
  LowByte = data[0];
  HighByte = data[1];
  TReading = (HighByte << 8) + LowByte;
  SignBit = TReading & 0x8000;  // test most sig bit
  if (SignBit) // negative
  {
    TReading = (TReading ^ 0xffff) + 1; // 2's comp
  }
  Tc_100 = (6 * TReading) + TReading / 4;    // multiply by (100 * 0.0625) or 6.25

  return Tc_100;
}

