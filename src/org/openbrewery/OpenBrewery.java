package org.openbrewery;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

import org.openbrewery.hardware.TemperatureSensor;
import org.openbrewery.services.LoggerThread;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;

/**
 ** Hybridmojo Brewery Controller **

 - ControlP5 and Arduino libraries are required

 - Designed for use with Arduino Mega2560

 - You need to specify which COM port to use

 (C) 2012, 2013 Peter Vieth, Hybridmojo LLC

 This file is part of the Hybridmojo Brewery Controller.

 The Hybridmojo Brewery Controller is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

 Hybridmojo Brewery Controller is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along with the Hybridmojo Brewery Controller. If not, see http://www.gnu.org/licenses/.

 */

//Import libraries
import controlP5.*;
import processing.serial.*;

//import cc.arduino.*;

public class OpenBrewery extends PApplet {
	// Specify which serial com port to use. See debug info on program start to
	// see options. In future hope to have an interactive way to do this.
	static int ARDUINO_COM_PORT_INDEX = 1; // normally 0
	
	public LoggerThread loggerThread = null;

	static String configFilename = "config.properties";

	// Force the program to work without hooking up an Arduino by setting the
	// next line to false. If set to true but no Arduino is connected, the
	// program will recognize this.
	static boolean ENABLE_ARDUINO_MODE = true;

	// Approximate analog to digital converter voltage (probably 5v unless you
	// have a 3.3v Arduino)
	static float ADC_voltage = 5000f; // in mV

	// Approximate voltage being supplied to valves, etc. For most industrial
	// automation equipment, this should be 24 volts (or 24000mV).
	static float VDD = 24000; // in mV

	Properties systemProperties = new Properties();

	/**
	 * Arduino related
	 **/

	Arduino arduino;

	/*
	 * Pin #s are a bit confusing for analog pins depending on which software or
	 * docs you are using. Analog Pin 0 = Pin 54, Analog Pin 1 = 55, and so
	 * on(PORTF and PORTK). Firmata_test application allows you to see these pin
	 * numbers and test them, but that's not how they're labeled on the official
	 * Arduinos themselves. The Processing Arduino library automatically adjusts
	 * pin #s when using analogRead to match the labeling on the board itself,
	 * so Analog Pin 0 is referenced as pin 0. We're using the Processing
	 * convention below.
	 */

	// Pins for discrete (on/off) valves.
	int[] onoff_valve_pins = { 8, 7, 6 }; // 9 is also available but unused for
											// now
	public int[] onoff_valve_state = { Arduino.LOW, Arduino.LOW, Arduino.LOW };

	/*
	 * Setup proportional valve pins. Most proportional valves work in a range,
	 * for example 2-10v or 0-10v, so a min and max are provided below. If you
	 * are using an N-channel FET to control the valves, the FET will have to be
	 * ON 100% to turn the valve OFF 100% so the default max below is less than
	 * the min.
	 */
	int[] valve_prop_pins = { 12, 13, 10 };
	public int[] valve_prop_state = { 0, 0, 0 };
	int[] valve_prop_min = { 220, 125, 125}; // 2v
	int[] valve_prop_max = { 125, 215, 220}; // 10v

	// Setup heater pins. Initialize to OFF.
	int[] heater_pins = { 2, 3, 4, 5 };
	public int[] heater_state = { Arduino.LOW, Arduino.LOW, Arduino.LOW, Arduino.LOW };

	// Setup pump pins. Initialize to OFF.
	int[] pump_pins = { 17, 15, 19 }; // 17 15  19
	public int[] pump_state = { Arduino.LOW, Arduino.LOW, Arduino.LOW };

	// Setup level sensing pins. Designed for use with a sensor like the
	// MPX5050, which takes 5v in and outputs an analog voltage.
	int[] level_sensor_pins = { 54 - 54, 56 - 54 };
	int[] level_sensor_state = { 0, 0 };

	// Level calculations. Calibrate by reading the level sensor values when the
	// tank is nearly empty and when full.
	float[] level_empty = { 25f, 25f }; // voltage when tanks are empty
	float[] level_full = { 750f, 750f }; // voltage when tanks are full
	public float[] level = { 0.0f, 0.0f };
	
	ArrayList<TemperatureSensor>tempSensors = new ArrayList<TemperatureSensor>();

	public ArrayList<TemperatureSensor> getTempSensors() {
		return tempSensors;
	}

	public void setTempSensors(ArrayList<TemperatureSensor> tempSensors) {
		this.tempSensors = tempSensors;
	}

	/*int[] temp_sensor_pins = { 58 - 54, 60 - 54, 62 - 54, 64 - 54 }; // A4 A6 A8
																		// A10
	int[] temperature_state = { 0, 0, 0, 0 };

	public float[] temperature = { 0.0f, 0.0f, 0.0f, 0.0f };
	// From datasheet... scale factor should be something like 30mV/C. The
	// offset is the voltage output @ a certain temperature, like 750mV at 25C.
	float[] temperature_scale_factor = { 30f, 30f, 30f, 30f }; // in mV/C
	float[] temperature_offset = { 22f, 22f, 22f, 22f }; // in C
	float[] temperature_offset_voltage = { 750f, 750f, 750f, 750f }; // in mV
*/
	/** GUI related declarations **/
	PImage bg; // Declare variable "a" of type PImage
	PFont fontA;

	ControlP5 cp5;
	ArrayList<Slider> prop_valve_slider = new ArrayList<Slider>();
	Toggle rawData;

	public void setup() {
		// Try to load properties file. If that fails, quit with an error.
		if (!loadPreferences(configFilename)) {
			System.exit(1);
		}

		// Setup some basic "constants"
		ADC_voltage = Float.parseFloat(systemProperties.getProperty(
				"arduino.adcVoltage", "5000"));
		VDD = Float.parseFloat(systemProperties.getProperty("power.vdd",
				"24000"));
		ENABLE_ARDUINO_MODE = Boolean.parseBoolean(systemProperties
				.getProperty("arduino.enable", "false"));
		System.out.println("Enable Arduino Mode? " + ENABLE_ARDUINO_MODE + "\n\n"
				+ "Vdd: " + VDD + ", ADC Voltage: " + ADC_voltage);

		if (ENABLE_ARDUINO_MODE) {
			setupPins();
			System.out.println("Starting logging... ");
			loggerThread = new LoggerThread(this,"eric",10000l);
			
			
			loggerThread.start();
			System.out.println("Logging started.");
		}

		
		
		size(1020, 730);
		// The jpg file must be in the data folder
		// of the current sketch to load successfully
		bg = loadImage("piping2.jpg"); // Load the image into the program

		/* FONT STUFF */
		smooth();
		// Load the font. Fonts must be placed within the data
		// directory of your sketch. A font must first be created
		// using the 'Create Font...' option in the Tools menu.
		fontA = loadFont("CourierNew36.vlw");
		ControlFont font = new ControlFont(fontA, 241);
		textAlign(CENTER);

		// Set the font and its size (in units of pixels)
		textFont(fontA, 16);
		
		// Get count of temp sensors
		int tempSensorCount = Integer.parseInt(systemProperties.getProperty(
				"brewery.sensor.temperature.count", "0"));
		System.out.println(tempSensorCount + " temperature sensors configured");
		
		// Load config for each temp sensor
		for(int i =1; i <= tempSensorCount; i++) {
			float c = Float.parseFloat(systemProperties.getProperty(
					"brewery.sensor.temperature."+i+".coefficient", "-90"));
			float offset = Float.parseFloat(systemProperties.getProperty(
					"brewery.sensor.temperature."+i+".offset", "-90"));
			String label = systemProperties.getProperty(
					"brewery.sensor.temperature."+i+".label", "Temp?");
			int pin = Integer.parseInt(systemProperties.getProperty(
					"brewery.sensor.temperature."+i+".pin", "0"));
			int x = Integer.parseInt(systemProperties.getProperty(
					"brewery.sensor.temperature."+i+".x", "0"));
			int y = Integer.parseInt(systemProperties.getProperty(
					"brewery.sensor.temperature."+i+".y", "0"));
			
			// Create new temp sensor object
			TemperatureSensor ts = new TemperatureSensor(pin, c, offset, label, x, y);
			System.out.println("Adding " + ts);
			
			// Add the new tempSensor object to the array of temperature sensors
			tempSensors.add(ts);
		}

		// Init CP5 GUI library (must be installed in libraries directory of
		// processing)
		cp5 = new ControlP5(this);
		rawData = cp5.addToggle("toggleCalculations").setPosition(20, 20)
				.setSize(50, 20).setCaptionLabel("Show Raw sensor Values");
		rawData.captionLabel().setFont(font).setSize(15).toUpperCase(false);

		/* Add sliders for the proportional valves */
		prop_valve_slider.add(cp5.addSlider("propval1")
				.setPosition(310 - 60, 370 - 50).setSize(100, 20)
				.setRange(0, 255).setNumberOfTickMarks(10).snapToTickMarks(false));
		prop_valve_slider.get(0).captionLabel().setFont(font).setSize(12)
				.setColor(0xff00ff00).toUpperCase(false);
		prop_valve_slider.get(0).addListener(
				new PropValveSliderListener(0, valve_prop_pins[0],
						valve_prop_state[0], "Valve 1", valve_prop_min[0],
						valve_prop_max[0]));

		prop_valve_slider.add(cp5.addSlider("propval2")
				.setPosition(600 - 40, 370 - 40).setSize(100, 20)
				.setRange(0, 255).setNumberOfTickMarks(10).snapToTickMarks(false));
		prop_valve_slider.get(1).addListener(
				new PropValveSliderListener(1, valve_prop_pins[1],
						valve_prop_state[1], "Valve 2", valve_prop_min[1],
						valve_prop_max[1]));

		prop_valve_slider.add(cp5.addSlider("propval3")
				.setPosition(830 + 80, 600 + 20).setSize(100, 20)
				.setRange(0, 255).setNumberOfTickMarks(10).snapToTickMarks(false));
		prop_valve_slider.get(2).addListener(
				new PropValveSliderListener(2, valve_prop_pins[2],
						valve_prop_state[2], "Valve 3", valve_prop_min[2],
						valve_prop_max[2]));

	}

	private Boolean loadPreferences(String configFilename2) {

		try {
			// URL startupPropsURL =
			// this.getClass().getClassLoader().getResource("/"+configFilename2);
			File startupPropsFl = new File(configFilename2); // startupPropsURL.toURI());

			try {
				InputStreamReader aRdr = new InputStreamReader(
						new FileInputStream(startupPropsFl));
				systemProperties.load(aRdr);
				aRdr.close();
			} catch (Throwable e) {
				System.out.println("Error occured while trying to load '"
						+ configFilename2 + "'");
				e.printStackTrace();
			}

			// System.out.println("Getting log files from "+tempProps.getProperty("foo.baseURL"));
			// return tempProps.getProperty("foo.baseURL");
		} catch (Throwable ex) {
			System.out.println("Error occured while trying to read from '"
					+ configFilename2 + "'");
			ex.printStackTrace();
			return false;
		}
		System.out.println("Loaded preferences file.");
		return true;
	}

	public void draw() {
		// Write pin states to Arduino
		if (ENABLE_ARDUINO_MODE) {
			// loopOutputs();
			getInputs();
		}

		// Displays the bg image at its actual size at point (0,0)
		image(bg, 0, 0);

		// Text color: red
		fill(255, 0, 0);

		// Write temps to screen
		
		// if the "show raw data" button is disabled, show the
				// calculated temperature
		for(int i=0; i < tempSensors.size(); i++) {
			// Get data for current temp sensor
			TemperatureSensor ts = tempSensors.get(i);
			
			// Decimation http://www.atmel.com/images/doc8003.pdf
			// sum 4^n samples to get n bits of resolution above 10 bits
			// finish by right shifting n times... same as dividing by 2^n
			Integer x = 0;
			// 16 = 4^2
			if(ENABLE_ARDUINO_MODE) {
				for(int a = 0; a < 16; a++) {
				    x = x + arduino.analogRead(ts.getPinNumber());
				}
				x = x / 4; // 2^2
			}
			
			//temperature_state[i] = x; // arduino.analogRead(temp_sensor_pins[i]);
			//float V = ((4095 - temperature_state[i]) / 4096f) * ADC_voltage;
			
			// Use coef * ln + offset to calculate temp.
			ts.setTemperature(ts.getCoefficient() * (new Float(Math.log(x))) + ts.getOffset()); 
			/*temperature_offset[i]
					+ (V - temperature_offset_voltage[i])
					/ temperature_scale_factor[i];*/
			if (rawData.getState()) { // if the "show raw data" button is enabled,
				// show the analog readings, not the
				// calculated temp
				printAnalogState(ts.getLabel(), x.toString(), ts.getX(), ts.getY());
			} else {
				printAnalogState(ts.getLabel(), String.format("%.1f%n", ts.getTemperature())+"", ts.getX(), ts.getY());
			}
		}
		

		// Write heater status
		printDigitalState("H1", heater_state[0], 180, 170);

		printDigitalState("H2", heater_state[1], 230, 170);

		printDigitalState("H3", heater_state[2], 750, 170);

		printDigitalState("H4", heater_state[3], 800, 170);

		// Write level status
		fill(0, 0, 255);
		if (rawData.getState()) {
			printAnalogState("level1", level_sensor_state[0], 480, 310);

			printAnalogState("level2", level_sensor_state[1], 800, 310);
		} else {
			printAnalogState("level1", level[0], 480, 310);

			printAnalogState("level2", level[1], 800, 310);
		}

		// Draw tank level
		fill(0, 0, 0);
		rect(505, 275, 40, -100);
		fill(100, 100, 255);
		rect(505, 275, 40, -level[0]);

		fill(0, 0, 0);
		rect(825, 275, 40, -100);
		fill(100, 100, 255);
		rect(825, 275, 40, -level[1]);

		// Write pump status
		fill(0, 0, 255);
		printDigitalState("pump1", pump_state[0], 200, 410);

		printDigitalState("pump2", pump_state[1], 520, 410);

		printDigitalState("pump3", pump_state[2], 830, 460);

		// Write prop valve status
		fill(0, 200, 0);

		printAnalogState("Prop1", valve_prop_state[0], 310, 370);

		printAnalogState("Prop2", valve_prop_state[1], 600, 370);

		printAnalogState("Prop3", valve_prop_state[2], 830, 600);

		// Write non-prop valve status
		printDigitalState("Valve1", onoff_valve_state[0], 130, 190);

		printDigitalState("Valve2", onoff_valve_state[1], 480, 540);

		printDigitalState("Valve3", onoff_valve_state[2], 940, 360);
	}

	/**
	 * Function to toggle a digital pin
	 **/
	int toggleState(int val, int pin, String debug_name) {
		if (val == Arduino.LOW) {
			val = Arduino.HIGH;
			print(debug_name + " ON");
		} else {
			val = Arduino.LOW;
			print(debug_name + " OFF");
		}
		if (ENABLE_ARDUINO_MODE) {
			arduino.digitalWrite(pin, val);
			print("\t written to Arduino pin " + pin);
		} else {
			print("\t NOT written to Arduino pin (no arduino mode ON)");
		}
		println("");
		return val;
	}

	/**
	 * It would be better to implement this via CP5 toggle buttons, but they're
	 * ugly :)
	 **/
	public void mousePressed() {

		// valves
		if (mouseX > 70 && mouseX < 160 && mouseY > 160 && mouseY < 220) {
			onoff_valve_state[0] = toggleState(onoff_valve_state[0],
					onoff_valve_pins[0], "Valve1");
		}

		if (mouseX > 380 && mouseX < 510 && mouseY > 510 && mouseY < 570) {
			onoff_valve_state[1] = toggleState(onoff_valve_state[1],
					onoff_valve_pins[1], "Valve2");
		}

		if (mouseX > 910 && mouseX < 970 && mouseY > 330 && mouseY < 430) {
			onoff_valve_state[2] = toggleState(onoff_valve_state[2],
					onoff_valve_pins[2], "Valve3");
		}

		// pumps
		if (mouseX > 200 - 30 && mouseX < 200 + 30 && mouseY > 410 - 30
				&& mouseY < 410 + 30) {
			pump_state[0] = toggleState(pump_state[0], pump_pins[0], "Pump1");
		}

		if (mouseX > 520 - 30 && mouseX < 520 + 30 && mouseY > 410 - 30
				&& mouseY < 410 + 30) {
			pump_state[1] = toggleState(pump_state[1], pump_pins[1], "Pump2");
		}

		if (mouseX > 830 - 30 && mouseX < 830 + 30 && mouseY > 460 - 30
				&& mouseY < 460 + 30) {
			pump_state[2] = toggleState(pump_state[2], pump_pins[2], "Pump3");
		}

		// heaters
		if (mouseX > 180 - 30 && mouseX < 180 + 30 && mouseY > 170 - 30
				&& mouseY < 170 + 30) {
			int i = 0;
			heater_state[i] = toggleState(heater_state[i], heater_pins[i],
					"Heater" + (i + 1));
		}

		if (mouseX > 230 - 30 && mouseX < 230 + 30 && mouseY > 170 - 30
				&& mouseY < 170 + 30) {
			int i = 1;
			heater_state[i] = toggleState(heater_state[i], heater_pins[i],
					"Heater" + (i + 1));
		}

		if (mouseX > 750 - 30 && mouseX < 750 + 30 && mouseY > 170 - 30
				&& mouseY < 170 + 30) {
			int i = 2;
			heater_state[i] = toggleState(heater_state[i], heater_pins[i],
					"Heater" + (i + 1));
		}

		if (mouseX > 800 - 30 && mouseX < 800 + 30 && mouseY > 170 - 30
				&& mouseY < 170 + 30) {
			int i = 3;
			heater_state[i] = toggleState(heater_state[i], heater_pins[i],
					"Heater" + (i + 1));
		}
	}

	/* Sliders dont update until released */
	public void mouseReleased() {
		// Detect changes to sliders, if there are changes, write to Arduino
		/*
		 * for(int i=0; i < valve_prop_pins.length; i++) {
		 * if(valve_prop_state[i]
		 * !=Math.round(prop_valve_slider.get(i).getValue())) {
		 * valve_prop_state[i]=Math.round(prop_valve_slider.get(i).getValue());
		 * println("Prop valve " + (i+1) + " set to " + valve_prop_state[i]);
		 * if(!NO_ARDUINO_MODE) { arduino.analogWrite(valve_prop_pins[i],
		 * Math.round(prop_valve_slider.get(i).getValue()));
		 * print("\t wrote changes to pin " + valve_prop_pins[i]); } else {
		 * print("\t did NOT write changes to pin " + valve_prop_pins[i]); }
		 * println(""); } }
		 */
	}

	/**
	 * This function just uses text() to print a label and a digital value at X
	 * and Y
	 **/

	void printDigitalState(String name, int value, int x, int y) {
		if (value == Arduino.LOW) {
			text(name + "\nOFF", x, y);
		} else {
			text(name + "\nON", x, y);
		}
		// line(mouseX,20,mouseX,500);
	}

	/**
	 * This function just uses text() to print a label and an analog value at X
	 * and Y
	 **/

	void printAnalogState(String name, String value, int x, int y) {
		text(name + "\n" + value, x, y);
	}

	void printAnalogState(String name, float value, int x, int y) {
		text(name + "\n" + value, x, y);
	}

	/**
	 * Configure the Arduino object and pins on that Arduino
	 **/
	void setupPins() {
		System.out.println("------ INIT ARDUINO -----");
		System.out.println("Available com ports: " + Arrays.toString(Arduino.list()));
		System.out.println("Config.properties says to use index " + Integer.parseInt(systemProperties
				.getProperty("arduino.comPortIndex")));
		ARDUINO_COM_PORT_INDEX = Integer.parseInt(systemProperties
				.getProperty("arduino.comPortIndex"));
		if ((Integer.parseInt(systemProperties
				.getProperty("arduino.comPortIndex")) + 1) > Arduino.list().length) {
			System.out
					.println("*** NO ARDUINO FOUND!  All Arduino features disabled.");
			ENABLE_ARDUINO_MODE = false;
			return;
		} else {
			System.out.println("Connecting to Arduino "
					+ systemProperties.getProperty("arduino.type", "mega2560")
					+ " on port "
					+ Arduino.list()[ARDUINO_COM_PORT_INDEX]
					+ " at "
					+ systemProperties.getProperty("arduino.comPortBaud",
							"57600") + " bps");
		}
		arduino = new Arduino(this, Arduino.list()[ARDUINO_COM_PORT_INDEX],
				Integer.parseInt(systemProperties.getProperty(
						"arduino.comPortBaud", "57600")),
				systemProperties.getProperty("arduino.type", "mega2560"));

		// Setup temperature
		System.out.println("\tInitializing temperature sensor pins...");
		for (int i = 0; i < tempSensors.size(); i++) {
			arduino.pinMode(tempSensors.get(i).getPinNumber(), Arduino.ANALOG);
			println("\to Setting A" + tempSensors.get(i).getPinNumber() + " to ANALOG");
		}

		// Setup level sense
		System.out.println("\tInitializing level sensor pins...");
		for (int i = 0; i < level_sensor_pins.length; i++) {
			arduino.pinMode(level_sensor_pins[i], Arduino.ANALOG);
			println("\to Setting A" + level_sensor_pins[i] + " to ANALOG");
		}

		// Setup normal valves
		System.out.println("\tInitializing on/off valve pins...");
		for (int i = 0; i < onoff_valve_pins.length; i++) {
			arduino.pinMode(onoff_valve_pins[i], Arduino.OUTPUT);
		}

		// Setup proportional valves
		System.out.println("\tInitializing proportional valve pins...");
		for (int i = 0; i < valve_prop_pins.length; i++) {
			arduino.pinMode(valve_prop_pins[i], Arduino.OUTPUT);
			// Initialize to closed state
			arduino.analogWrite(valve_prop_pins[i], Math.round(map(0, 0, 255,
					valve_prop_min[i], valve_prop_max[i])));
			//arduino.analogWrite(valve_prop_pins[i], Math.round(map(0, 0, 255, 
			//		valve_prop_min[i], valve_prop_max[i])));
		}

		// Setup normal heaters
		System.out.println("\tInitializing heater on/off pins...");
		for (int i = 0; i < heater_pins.length; i++) {
			arduino.pinMode(heater_pins[i], Arduino.OUTPUT);
		}

		// Setup normal pumps
		System.out.println("\tInitializing pump on/off pins...");
		for (int i = 0; i < pump_pins.length; i++) {
			arduino.pinMode(pump_pins[i], Arduino.OUTPUT);
		}

		System.out.println("------ INIT COMPLETE -----");
	}

	/** UNUSED **/
	void loopOutputs() {
		// Setup proportional valves
		for (int i = 0; i < onoff_valve_pins.length; i++) {
			arduino.digitalWrite(onoff_valve_pins[i], onoff_valve_state[i]);
		}

		// Setup pumps
		for (int i = 0; i < pump_pins.length; i++) {
			arduino.digitalWrite(pump_pins[i], pump_state[i]);
		}

		// Setup pumps
		for (int i = 0; i < heater_pins.length; i++) {
			arduino.digitalWrite(heater_pins[i], heater_state[i]);
		}
	}

	/**
	 * This function gets the temperature and level sensor values and calculates
	 * temperature in C and levels in %.
	 **/
	void getInputs() {
		/*for (int i = 0; i < temp_sensor_pins.length; i++) {
			// Decimation http://www.atmel.com/images/doc8003.pdf
			// sum 4^n samples to get n bits of resolution above 10 bits
			// finish by right shifting n times... same as dividing by 2^n
			Integer x = 0;
			// 16 = 4^2
			for(int a = 0; a < 16; a++) {
			    x = x + arduino.analogRead(temp_sensor_pins[i]);
			}
			x = x / 4; // 2^2
			
			temperature_state[i] = x; // arduino.analogRead(temp_sensor_pins[i]);
			float V = ((4095 - temperature_state[i]) / 4096f) * ADC_voltage;
			temperature[i] = temperature_offset[i]
					+ (V - temperature_offset_voltage[i])
					/ temperature_scale_factor[i];
		}*/

		// Since the voltage read from the differential pressure sensor is
		// linear but the valves are dependent on the ambient conditions as well
		// as the tank dimensions, it is better to map ADC readings to the empty
		// and full states and interpolate.
		for (int i = 0; i < level_sensor_pins.length; i++) {
			level_sensor_state[i] = arduino.analogRead(level_sensor_pins[i]);
			level[i] = map(level_sensor_state[i], level_empty[i],
					level_full[i], 0, 100);
			// float V = (temperature_state[i]/1024f) * ADC_voltage;
			// temperature[i]=temperature_offset[i]+(V-temperature_offset_voltage[i])/temperature_scale_factor[i];
		}
	}

	/**
	 * This is the listener for slider change events. The change event appears
	 * to get fired multiple times per click. Hmm. The constructor requires
	 * information about the valve to be actuated.
	 **/
	class PropValveSliderListener implements ControlListener {
		int pin;
		String name;
		int index;
		int valve_prop_pos;
		int valve_prop_min;
		int valve_prop_max;

		public PropValveSliderListener(int index, int pinNumber, int valve_prop_state,
				String name, int min_val, int max_val) {
			super();
			this.index = index;
			this.pin = pinNumber;
			this.name = name;
			this.valve_prop_pos = valve_prop_state;
			this.valve_prop_min = min_val;
			this.valve_prop_max = max_val;
		}

		public void controlEvent(ControlEvent theEvent) {
			valve_prop_pos = Math.round(theEvent.getController().getValue());
			println(this.name + " set to " + valve_prop_state);
			if (ENABLE_ARDUINO_MODE) {
				arduino.analogWrite(pin, Math.round(map(theEvent
						.getController().getValue(), 0, 255, valve_prop_min,
						valve_prop_max)));
				print("\t wrote changes to pin " + pin);
				valve_prop_state[this.index] = valve_prop_pos;
			} else {
				print("\t did NOT write changes to pin " + pin);
			}
			println("");
		}
	}
	
	/*private void prepareExitHandler(LoggerThread loggerThread) {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			public void run () {
				//System.out.println("SHUTDOWN HOOK");
				try {
					if(this.loggerThread!=null) {
						this.loggerThread.cancel();
						Thread.sleep(100);
					}
					stop();
				} catch (Exception ex){
					ex.printStackTrace(); // not much else to do at this point
				}
							
			}
		}));
	}*/

	public static void main(String _args[]) {
		PApplet.main(new String[] { org.openbrewery.OpenBrewery.class.getName() });
	}
	
		 
}
