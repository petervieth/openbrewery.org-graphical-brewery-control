package org.openbrewery.hardware;

public class TemperatureSensor {
	int pinNumber;
	float temperature;
	
	float offset;
	float coefficient;
	String label;
	int x;
	int y;
	public TemperatureSensor(int pinNumber, float coefficient, float offset, 
			String label, int x, int y) {
		super();
		this.pinNumber = pinNumber;
		this.offset = offset;
		this.coefficient = coefficient;
		this.label = label;
		this.x = x;
		this.y = y;
	}
	public int getPinNumber() {
		return pinNumber;
	}
	public void setPinNumber(int pinNumber) {
		this.pinNumber = pinNumber;
	}
	public float getOffset() {
		return offset;
	}
	public void setOffset(float offset) {
		this.offset = offset;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public float getTemperature() {
		return temperature;
	}
	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}
	public void setCoefficient(float coefficient) {
		this.coefficient = coefficient;
	}
	
	public float getCoefficient() {
		return this.coefficient;
	}
	
	public String toString() {
		return "Temperature Sensor: [pin:"+this.pinNumber+", equation: " + this.coefficient+"*ln(x)+"+this.offset+", label:" + this.label + ", x:" + this.x + ", y:" +this.y;
	}
}
