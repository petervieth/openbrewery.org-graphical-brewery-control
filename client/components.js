// Styles for states
var offColor = "panel-default";
var onColor = "panel-success";

function jsonp(stuff) {
	return stuff;
}

//Self-Executing Anonymous Func: Part 2 (Public & Private) 
// define a function, then call it immediately 
$().ready(function() {
(function( openbrewery, $, _, undefined ) {
    //Private Property
    //var isHot = true;

    //Public Properties
	
	
	// sub modules
	openbrewery.tools = {};
	openbrewery.components = {};
	openbrewery.views = {};
	
	openbrewery.components.arduino = {};
	
	
	/* 
		Reusable helper functions
	*/
	
	openbrewery.tools.generateUniqueName = function(model)	{
		if(!model.get("name")) {
			model.set("name", model.cid);
		}
	},
	
	openbrewery.tools.dummySave = function(attrs, model) {
		/*if(model.hasIo()) 
			console.log("[" + model.get("tag")+".save] Contacting Arduino...");
		else
			console.log("[" + model.get("tag")+".save] No IO device has been associated with this component ("+model.get("name")+").");
		
		// If the value is being updated and an io device is associated, send the attributes to update and a reference to this object.  This allows us to postpone updating the object until the save is completed.
		// If no io device is associated, set immediately.  
		var ioList = model.getIo();
		for(var i=0; i<ioList.length; i++) {
			if(ioList[i]!=null) {
				var name = model.get("name");
				console.log(name + " saving update to  '" + name +"' to IO '" + ioList[i].name+"'");
				
				// If a save function is present (output), call it
				if(ioList[i].save)
					ioList[i].save(attrs);
			}
		}*/
		model.save(attrs);
	},
	
		
	/*
		Backbone needs models and associated views.  Models store data, views capture info on how to display this data.  The actual HTML templates are within the main HTML.
		For the brewery components such as tanks, there are corresponding views for each model.  For arduino parts (like pins), there are no views since these parts aren't actually displayed.
		
		Models can also sync to web services/APIs.  Only the arduino related models sync to a web service (located in the Atmel ATMega portion of the Yun). The brewery is not synced to any web service.
		
		The brewery and all connections to the arduino are configured in openbrewery.js
		
	*/
	
	openbrewery.components.brewery = Backbone.Model.extend({
		defaults: {
			photo: "/img/placeholder.png",
			name:"brewery"+(Math.random() + 1).toString(36).substring(10),
			prettyName: "default brewery",
			components: [],
			arduino: null // We also need an arduino, this must be set after initialization since we need the arduino to load first via REST calls
		},
		
		initialize: function () {
			console.log("[openbrewery.components.brewery.initialize] Creating " + this.get("name") + " associated with Arduino " + this.get("arduino").get("name"));
		},
		
		/* Helper function to add components to the brewery.  Include model, view, name, etc in args.
			Adds a new component to the list of brewery components and also renders it view the view provided.  
		*/
		addComponent: function(args) {			
			var newComponent = new args.model(args.modelProperties);
			//console.log(newComponent);
			var componentView = new args.view({
				model: newComponent,
				id: newComponent.get("name") // TODO: is this OK?
			});
			
			// Save component in array TODO: this isn't actually used, delete?
			//this.attributes.components.push({model: newComponent, view: componentView});
			var domEl = componentView.render().el;
			args.container.append(domEl);
			return $("#"+newComponent.attributes.name);
		}
	});
	
	openbrewery.views.brewery = Backbone.View.extend({

		// What type of element should the template be created in?
		tagName: "div",

		// Class name for the view
		className: "brewery component",

		// Set the template for the view
		template: _.template($('#breweryTemplate').html()),

		// Define events, ie, button clicks
		events: {
		   //"click .toggleStatus": "toggleStatus"
		},
		
		initialize: function () {
			//this.listenTo(this.model, "change", this.render); A name change is really the only feasible change... unless we start adding multiple arduinos
			this.model.on('change', this.render, this);
			//this.model.on('remove', this.unrender, this); TODO: add unrender.  Is that really necessary? Low priority
		   this.render();
		},

		render: function () {
			// Get HTML from underscore using this model's properties
			var markup = this.template(this.model.toJSON());
			this.$el.html(markup);
			return this; // return the created element
		}
	});
  
	
	/* 
		Arduino Model
		note, digitalPin etc aren't nested under arduino because we run into issues with defining arduino blowing away anything underneath it.  but the pins must be defined first or the collections in arduino dont work.
	*/
	
	openbrewery.components.arduino.board = Backbone.Model.extend({
		defaults: {
			photo: "/img/arduino.png",
			name:"mega",
			prettyName: "default arduino",
			ip: 'localhost',
			port: 8887,
			protocol: "http",
			io: null			
		},
		initialize: function() {
			console.log("[Arduino Model] initialize");
			
			var that = this;
			
			this.set("io",new BO.IOBoard(this.get("ip"), this.get("port"), this.get("protocol")));

		},
		

		
		getBaseUrl: function() {
			return this.get("protocol") + "://" + this.get("ip") + ":" + this.get("port") + "/arduino"; 
		},
		

	});
	
	/*
		Pump
	*/
	
    openbrewery.components.pump = Backbone.Model.extend({
		defaults: {
			photo: "/img/placeholder.png",
			color: offColor,
			prettyName: "default pump",
			status:false,
			io: null,
			board:null,
			tag: "openbrewery.components.pump"
		},
		initialize: function() {
			// If user provided no name, assign one
			openbrewery.tools.generateUniqueName(this);
			
			if(this.get("initialize"))
				this.get("initialize")(this);
			if(this.get("save"))
				this.save=this.get("save");
		},
		hasIo: function() {
			return (this.get("io") ? true: false);
		},
		/*
			Override Backbone's default save (which calls sync, FYI).  This dummy function only sets the attributes.  If IO is present, please replace with an appropriate function to set not only the model itself, but update the state of any input/output associated with this component.
		*/ 
		save: function(attrs) {
			this.set(attrs);
		},
		turnOn: function () { // on is reserved
			
			if(this.hasIo()) {
				console.log("[openbrewery.components.pump.turnOn] Contacting Arduino to enable digital pin...");
				
			} else
				console.log("[openbrewery.components.pump.turnOn] No IO device has been associated with this component ("+this.get("name")+"). NOT physically turning on anything as a result");
			
			this.save({
					status: true,
					color: onColor
				}, {wait:true});
		},
		turnOff: function () {
			if(this.hasIo()) {
				console.log("[openbrewery.components.pump.turnOff] Contacting Arduino to turn off digital pin...");
			} else
				console.log("[openbrewery.components.pump.turnOff] No IO device has been associated with this component ("+this.get("name")+"). NOT physically turning off anything as a result");
			this.save({
				status: false,
				color: offColor
			}, {wait:true});
		},
		isOn: function () {
			return this.get('status');
		}
	});
	
	openbrewery.views.pump = Backbone.View.extend({

		// What type of element should the template be created in?
		tagName: "div",

		// Class name for the view
		className: "pump component",

		// Set the template for the view
		template: _.template($('#pumpTemplate').html()),

		events: {
		   "click .toggleStatus": "toggleStatus"
		},
		
		toggleStatus: function() {
			if(this.model.isOn())
				this.model.turnOff();
			else
				this.model.turnOn();
		},

		initialize: function () {
			//this.listenTo(this.model, "change", this.render);
			this.model.on('change', this.render, this);
			//this.model.on('remove', this.unrender, this);
			this.render();
		},

		render: function () {
			// Set fixed CSS attributes, if provided
			if(this.model.get("css")) {
				this.$el.css(this.model.get("css"));
			}
			
			//this.$el.html("<span>" + this.model.color + "</span>");
			//console.log("rendering " + this.model.attributes.name);
			//return this;

			var markup = this.template(this.model.toJSON());
			this.$el.html(markup);
			//this.$el.toggleClass('done', this.model.get('done'));
			//this.input = this.$('.edit');
			return this; // return the created element
		}
	});
	
	/*
		Valve (on/off)
		
		defaults: default values for initialization.  Keeps the HTML template from breaking amoung other things.
		initialize: called when the object is instantiated.  
		save: saves the valve itself AND updates any associated IO
		turnOn: turns the valve on.  Updates model and associated IO
		turnOff: ditto except turns valve off
		isOn: get the state of the valve
	*/
	openbrewery.components.valve = Backbone.Model.extend({
		defaults: {
			photo: "/img/placeholder.png",
			color: offColor,
			prettyName: "default on off valve",
			status:false,
			io: null,
			board: null,
			tag: "openbrewery.components.valve"
		},
		initialize: function() {
			// If user provided no name, assign one
			openbrewery.tools.generateUniqueName(this);
			
			if(this.get("initialize"))
				this.get("initialize")(this);
			if(this.get("save"))
				this.save=this.get("save");
		},
		hasIo: function() {
			return (this.get("io") ? true: false);
		},
		/*
			Override Backbone's default save (which calls sync, FYI).  This dummy function only sets the attributes.  If IO is present, please replace with an appropriate function to set not only the model itself, but update the state of any input/output associated with this component.
		*/ 
		save: function(attrs) {
			this.set(attrs);
		},
		
		/* custom methods */
		turnOn: function () { // on is reserved
			
			if(this.hasIo()) {
				console.log("[openbrewery.components.valve.turnOn] Contacting Arduino to enable digital pin...");
				
			} else
				console.log("[openbrewery.components.valve.turnOn] No IO device has been associated with this component ("+this.get("name")+"). NOT physically turning on anything as a result");
			
			this.save({
					status: true,
					color: onColor
				}, {wait:true});
		},
		turnOff: function () {
			if(this.hasIo()) {
				console.log("[openbrewery.components.valve.turnOff] Contacting Arduino to turn off digital pin...");
			} else
				console.log("[openbrewery.components.valve.turnOff] No IO device has been associated with this component ("+this.get("name")+"). NOT physically turning off anything as a result");
			this.save({
				status: false,
				color: offColor
			}, {wait:true});
		},
		isOn: function () {
			return this.get('status');
		}
	});
	
	openbrewery.views.valve = Backbone.View.extend({

		// What type of element should the template be created in?
		tagName: "div",

		// Class name for the view
		className: "valve component",

		// Set the template for the view
		template: _.template($('#valveTemplate').html()),

		// Define events, ie, button clicks
		events: {
		   "click .toggleStatus": "toggleStatus"
		},
		
		toggleStatus: function() {
			if(this.model.isOn())
				this.model.turnOff();
			else
				this.model.turnOn();
		},

		initialize: function () {
			//this.listenTo(this.model, "change", this.render);
			this.model.on('change', this.render, this);
			//this.model.on('remove', this.unrender, this);
		   this.render();
		},

		render: function () {
			// Set fixed CSS attributes, if provided
			if(this.model.get("css")) {
				this.$el.css(this.model.get("css"));
			}

			var markup = this.template(this.model.toJSON());
			this.$el.html(markup);
			return this; // return the created element
		}
	});	
	
	/*
		Heater
	*/
	openbrewery.components.heater = Backbone.Model.extend({
		defaults: {
			photo: "/img/placeholder.png",
			color: offColor,
			prettyName: "default heater",
			status:false,
			io: null,
			tag: "openbrewery.components.heater"
		},
		initialize: function() {
			// If user provided no name, assign one
			openbrewery.tools.generateUniqueName(this);
			if(this.get("initialize"))
				this.get("initialize")(this);
			if(this.get("save"))
				this.save=this.get("save");
		},
		hasIo: function() {
			return (this.get("io") ? true: false);
		},
		/*
			Override Backbone's default save (which calls sync, FYI).  This dummy function only sets the attributes.  If IO is present, please replace with an appropriate function to set not only the model itself, but update the state of any input/output associated with this component.
		*/ 
		save: function(attrs) {
			this.set(attrs);
		},
		
		/* Custom methods */
		turnOn: function () { // on is reserved
			this.save({
					status: true,
					color: onColor
				}, {wait:true});
		},
		turnOff: function () {
			this.save({
				status: false,
				color: offColor
			}, {wait:true});
		},
		isOn: function () {
			return this.get('status');
		}
	});
	
	openbrewery.views.heater = Backbone.View.extend({

		// What type of element should the template be created in?
		tagName: "div",

		// Class name for the view
		className: "heater component",

		// Set the template for the view
		template: _.template($('#heaterTemplate').html()),

		events: {
		   "click .toggleStatus": "toggleStatus"
		},
		
		toggleStatus: function() {
			if(this.model.isOn())
				this.model.turnOff();
			else
				this.model.turnOn();
		},

		initialize: function () {
			//this.listenTo(this.model, "change", this.render);
			this.model.on('change', this.render, this);
			//this.model.on('request', this.showLoading, this);
			//this.model.on('sync', this.hideLoading, this);
			//this.model.on('remove', this.unrender, this);
		   this.render();
		},

		render: function () {
			// Set fixed CSS attributes, if provided
			if(this.model.get("css")) {
				this.$el.css(this.model.get("css"));
			}
			
			var markup = this.template(this.model.toJSON());
			this.$el.html(markup);
			return this; // return the created element
		},
		
		showLoading: function() {
			this.$el.addClass("loading");
		},
		
		hideLoading: function() {
			this.$el.removeClass("loading");
		}
	});

	/*
		Valve (proportional)
	*/
	
	// Define a proportional valve
	openbrewery.components.proportionalValve = Backbone.Model.extend({
		defaults: {
			photo: "/img/placeholder.png",
			color: offColor,
			prettyName: "default prop valve",
			status:0,
			minPWM:0,
			maxPWM:255,
			currentPWM:0,
			minOpen:0, // %
			maxOpen:100,
			io: null,
			tag: "openbrewery.components.proportionalValve"
		},
		
		initialize: function() {
			// If user provided no name, assign one
			openbrewery.tools.generateUniqueName(this);
			if(this.get("initialize"))
				this.get("initialize")(this);
			if(this.get("save"))
				this.save=this.get("save");
		},
		hasIo: function() {
			return (this.get("io") ? true: false);
		},
		/*
			Override Backbone's default save (which calls sync, FYI).  This dummy function only sets the attributes.  If IO is present, please replace with an appropriate function to set not only the model itself, but update the state of any input/output associated with this component.
		*/ 
		save: function(attrs) {
			this.set(attrs);
		},
		
		turnOn: function (amount) {
			console.log("Setting prop valve " + this.name + " to amount: " + amount + "%");
			var minOpen = this.get("minOpen");
			var maxOpen = this.get("maxOpen");
			var maxPWM = this.get("maxPWM");
			var minPWM = this.get("minPWM");
			
			if(amount<=minOpen ) {
				amount=minOpen;
			} else if(amount > maxOpen) {
				amount=maxOpen;
			} 
			var currentPWM = (((amount-minOpen)/maxOpen)); 
			console.log("Attempted to set prop valve " + this.get("name") + " to PWM: " + currentPWM);
			this.save({
					status: amount,
					currentPWM: currentPWM,
					color: (status==this.get("minOpen") ? offColor : onColor)
				});
				
			
		},
		
		turnOff: function () {
			this.save({
					status: 0,
					color: offColor
				});
		},
		
		isOn: function () {
			return this.get('status');
		},
		
		validate: function(attributes){
		  if ( attributes.status <  0 || attributes.status > 1){
			  return 'Status must be between 0 and 1';
		  }

		  if ( !attributes.name ){
			  return 'Every valve must have a name.';
		  }
		}
	});
	
	openbrewery.views.proportionalValve = Backbone.View.extend({

		// What type of element should the template be created in?
		tagName: "div",

		// Class name for the view
		className: "propValve component",

		// Set the template for the view
		template: _.template($('#propValveTemplate').html()),

		events: {
			"click .open5": "open5",
			"click .close5": "close5"
		   /*     "click .button.delete": "destroy"*/
		},
		
		open5: function() {
			console.log("open by 5% to " + this.model.get('status')+5);
			this.model.turnOn(this.model.get('status')+5);
		},
		close5: function() {
			console.log("closing by 5%");
			this.model.turnOn(this.model.get('status')-5);
		},

		initialize: function () {
			//this.listenTo(this.model, "change", this.render);
			this.model.on('change', this.render, this);
			//this.model.on('remove', this.unrender, this);
		   this.render();
		},

		render: function () {
			// Set fixed CSS attributes, if provided
			if(this.model.get("css")) {
				this.$el.css(this.model.get("css"));
			}

			var markup = this.template(this.model.toJSON());
			this.$el.html(markup);
			
			// Update the progress bar
			var progressbar = this.$('.progress-bar');
			progressbar.css('width', this.model.get('status')+'%').attr('aria-valuenow', this.model.get('status'));

			return this; // return the created element
		}
	}); 
	
	
	
		/*
		heatExchanger (only reads temp)
	*/
	// Define a valve (on/off),
	openbrewery.components.heatExchanger = Backbone.Model.extend({
		defaults: {
			photo: "/img/placeholder.png",
			color: offColor,
			prettyName: "default heat ex",
			ioTemperature: null,
			temperature:-1,
			tag: "openbrewery.components.heatExchanger"
		},

		initialize: function() {
			// If user provided no name, assign one
			openbrewery.tools.generateUniqueName(this);
			if(this.get("initialize"))
				this.get("initialize")(this);
			//that.listenTo(that.get("ioTemperature"), "change", that.changeTemperature);
		},
		hasIo: function() {
			return (this.get("ioTemperature") ? true: false);
		},

		// Change listener for temp. This is merely for reuse since there's no way to update the model from inside
		onTemperatureChange: function(event) {
			// The potentiometer gives back a value between 0 and 1.0
			// The TMP36 measures temperature in degrees Celsius
			// 10 mV = 1 degree Celsius
			// there is an offset of -500 mV to account for negative temperatures
			// 0 degrees Celsius = 500 mv.
			var val = event.target.value*4096;
			return (-64.84 * Math.log(val) + 573).toFixed(2);
			var milliVolts = val * 1000 * 5; // TODO; don't hard code 5v
			var tempC = (milliVolts - 500) / 10;
			var tempF = tempC * (9/5) + 32;
			
			return tempF.toFixed(0);
			//this.set("temperature", tempF.toFixed(0));
		}
		
	});
	
	openbrewery.views.heatExchanger = Backbone.View.extend({

		// What type of element should the template be created in?
		tagName: "div",

		// Class name for the view
		className: "heatEx component",

		// Set the template for the view
		template: _.template($('#heatExchangerTemplate').html()),

		// Define events, ie, button clicks
		events: {
		   
		},
		
		initialize: function () {
			//this.listenTo(this.model, "change", this.render);
			this.model.on('change', this.render, this);
			//this.model.on('remove', this.unrender, this);
		   this.render();
		},

		render: function () {
			// Set fixed CSS attributes, if provided
			if(this.model.get("css")) {
				this.$el.css(this.model.get("css"));
			}
			
			var markup = this.template(this.model.toJSON());
			this.$el.html(markup);

			return this; // return the created element
		}
	});
	
	
	/*
		Tank
	*/
	// Define a tank
	openbrewery.components.tank = Backbone.Model.extend({
		defaults: {
			photo: "/img/placeholder.png",
			color: offColor,
			prettyName: "default tank",
			status:0,
			level:0,
			levelPercent:0,
			temperature:0,
			minLevel:0,
			maxLevel:1,
			lowLevel:.1,
			highLevel:.9,
			ioLevel: null,
			ioTemperature: null,
			tag: "openbrewery.components.tank"
		},
		
		initialize: function() {
			// If user provided no name, assign one
			openbrewery.tools.generateUniqueName(this);
			if(this.get("initialize"))
				this.get("initialize")(this);
			if(this.get("save"))
				this.save=this.get("save");
		},

		
		// Change listener for level. This is merely for reuse since there's no way to update the model from inside
		onLevelChange: function(event) {	
			this.set("levelPercent", Math.floor(event.target.value*100));
			return event.target.value; // 0-1 it seems //Math.floor(((event.target.value-this.get('minLevel'))/(this.get('maxLevel')-this.get('minLevel')))*100);
		},
		
		// Change listener for temp. This is merely for reuse since there's no way to update the model from inside
		onTemperatureChange: function(event) {
			// The potentiometer gives back a value between 0 and 1.0
			// The TMP36 measures temperature in degrees Celsius
			// 10 mV = 1 degree Celsius
			// there is an offset of -500 mV to account for negative temperatures
			// 0 degrees Celsius = 500 mv.
			
			var val = event.target.value *4096;
			var milliVolts = val * 1000 * 5; // TODO; don't hard code 5v
			
			return (-64.84 * Math.log(val) + 573).toFixed(2);
			// with decimation return -64.84 * Maht/log(val) + 573;
			
			var tempC = (milliVolts - 500) / 10;
			var tempF = tempC * (9/5) + 32;
			
			return tempF.toFixed(0);
			//this.set("temperature", tempF.toFixed(0));
		},
		
		// Change listener for temp
		updateTemperature: function(temp) {
			this.set("temperature", temp);
		},
		
		hasIo: function() {
			return ((this.get("ioLevel") || this.get("ioTemperature")) ? true: false);
		},
		
		getIo:function() {
			return [this.get("ioLevel"), this.get("ioTemperature")];
		},
		
		// force tank level
		setLevel: function (amount) {
			
			if(amount<=this.get('maxLevel') && amount >0) {
				this.set({
					status: amount,
					color: onColor
				});
			} else if(amount > this.get('maxLevel')) {
				this.set({
					status: this.get('maxLevel'),
					color: onColor
				});
			} else {
				this.set({
					status: 0,
					color: offColor
				});
			}
			console.log("Attempted to setting tank level " + this.name + " to amount: " + amount + " actual: "+this.get('status'));
			
		}
	});

	openbrewery.views.tank = Backbone.View.extend({

		// What type of element should the template be created in?
		tagName: "div",

		// Class name for the view
		className: "tank component",

		// Set the template for the view
		template: _.template($('#tankTemplate').html()),

		events: {
			"click .fill5": "fill5",
			"click .empty5": "empty5"
		},
		
		fill5: function() {
			console.log("fill by 5% to " + this.model.get('status')+50);
			this.model.setLevel(this.model.get('status')+50);
			if(this.model.get('status') >= this.model.get('highLevel'))
				toastWarning(this.model.get('name')+' level is high');
			else if (this.model.get('status') <= this.model.get('lowLevel'))
				toastWarning(this.model.get('name')+' level is low');
		},
		empty5: function() {
			console.log("empty by 5%");
			this.model.setLevel(this.model.get('status')-50);
		},

		initialize: function () {
	   
			/*if(this.model.get("ioLevel"))
				this.listenTo(this.model.get("ioLevel"), "change", this.render);
			if(this.model.get("ioTemperature"))
				this.listenTo(this.model.get("ioTemperature"), "change", this.render);*/
			//this.listenTo(this.model, "change", this.render);
			this.model.on('change', this.render, this);
			//this.model.on('remove', this.unrender, this);
		   this.render();
		},

		render: function () {
			//console.log("TANK RENDER " + Math.floor(((this.model.get('status')-this.model.get('minLevel'))/(this.model.get('maxLevel')-this.model.get('minLevel')))*100)+'%');
			// Set fixed CSS attributes, if provided
			if(this.model.get("css")) {
				this.$el.css(this.model.get("css"));
			}
			
			// Get template html using the model from underscore
			var markup = this.template(this.model.toJSON());
			this.$el.html(markup);
			
			// Update the progress bar
			var progressbar = this.$('.progress-bar');
			
			var level = this.model.get("level");
			//console.log(new Date() + "TANK LEVEL IS " + this.model.get('status'));
			// Underscore doesn't seem to update progressbar correctly on its own.  Or maybe just mainting aria-valuenow doesn't trigger the progressbar to update itself, so have to set css width here.
			progressbar.css('width', Math.floor(level*100)+'%'); //.attr('aria-valuenow', this.model.get('status'));
			
			// Warn if low or high level
			if(level<=this.model.get('lowLevel') || level>=this.model.get('highLevel'))
				progressbar.addClass('progress-bar-warning');
			else
				progressbar.removeClass('progress-bar-warning');
			return this; // return the created element
		}
	});
	
}( window.openbrewery = window.openbrewery || {}, jQuery, _ ));


// || {} makes sure we're not overwriting "namespace". 
});