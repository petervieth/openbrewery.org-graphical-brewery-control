# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

## Quick summary ##

OpenBrewery allows control of a model nanobrewery using 3 tanks, 7 valves, 3 pumps, and 4 electric heating elements.  It is intended to be used in a residential setting (240v 80 amp service).  The software has been tested with a 55 gallon nanobrewery.  The project page can be found at openbrewery.org.
### Version ###
2.x contains the latest software; it is a web browser control panel which uses BreakoutJS to communicate with an Arduino Mega running Advanced Firmata.  v1.0 used Processing and standard firmata to control the brewery from a desktop client.
## What's included? ##
The source code contains the following folders:
* client - Javascript+HTML for using Backbone and BreakoutJS
* firmware - Arduino AdvancedFirmata firmware
* server - BreakoutJS server binaries
* hardware - schematics for an Arduino shield (not yet ready as of 6/6/2015)
* OnewireBridge - since Firmata does not support the common onewire temperature sensors, a Teensy (pjrc.com) or other Arduino can be used as a bridge to generate analog voltages which AdvancedFirmata can handle

## How do I get set up? ##

### Summary of set up ###
* Build the brewery (see OpenBrewery.org)
* Download the project and extract.
*Install AdvancedFirmata (found in the firmware directory) onto an Arduino Mega2560 using Arduino IDE.
* Start the appropriate version of Breakout-Server on the server machine.  Make sure the server's root directory is pointing to the client directory downloaded earlier.  32 bit versions will run on 64bit systems, FYI.
* Use the Breakout-Server control panel to choose the correct serial com port and then click the connect button to connect to the Mega2560.
* Access the brewery web control panel by opening a browser on the server to http://localhost:8887.

## Configuration ##
Brewery configuration is in Openbrewery.js.  This builds a brewery programmatically using Javascript.  
### Dependencies ###
Dependencies are included in the project package. You should not need to download anything other than the Arduino IDE and probably Google Chrome.
### How to run tests ###
Testing should be conducted by refreshing the web control panel.  Do not power up the 120v and 240v portions of your brewery.  If using off the shelf relay boards, logic level power should be enough to power the LEDs present on these boards.  Use these LEDs to verify proper operation before powering up the high voltage side of the brewery.
### Deployment instructions ###
Testing was performed on a Windows XP PC and Windows 8.  Advanced Firmata was pushed to a Arduino Mega 2560 using Arduino 1.5.8.
Latest Chrome as of 6/6/2015 was used for testing of the web control panel.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin:
Contact Peter Vieth for project access.  Peter at vieth.pl.
* Other community or team contact: 
For inquiries regarding brewery hardware, contact Eric Anzelc eanzelc at gmail.